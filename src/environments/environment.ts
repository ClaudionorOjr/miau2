// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD5iAttIsmpBkFmtn2oXaYaRoVgj_xtSmk",
    authDomain: "miau-d2cde.firebaseapp.com",
    databaseURL: "https://miau-d2cde.firebaseio.com",
    projectId: "miau-d2cde",
    storageBucket: "miau-d2cde.appspot.com",
    messagingSenderId: "430190398115",
    appId: "1:430190398115:web:e701c94a509f1d2afe4d16"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
